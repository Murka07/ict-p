public class Rectangle extends GObj {
    private double w;
    private double h;

    public Rectangle(double w, double h){
        this.w = w;
        this.h = h;
    }
    public Rectangle(double w, double h , String color, boolean filled){
        this.w = w;
        this.h = h;
        setColor(color);
        setFilled(filled);
    }
    public double getW(){
        return w;
    }
    public double getH(){
        return h;
    }
    public void setW(double w){
        this.w = w;
    }
    public void setH(double h){
        this.h = h;
    }
    public double getArea(){
        return w * h;
    }
}
